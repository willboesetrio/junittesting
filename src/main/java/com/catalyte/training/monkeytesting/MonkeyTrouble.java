package com.catalyte.training.monkeytesting;

/**
 * @author Will Boese
 * @version 1.0
 * CP-C0 unit testing assignment 12/2022
 */

public class MonkeyTrouble {

  /**
   * Tests if there is monkey trouble according to assignment instructions
   * @param aSmile boolean
   * @param bSmile boolean
   * @return boolean
   */
  public boolean monkeyTrouble(boolean aSmile, boolean bSmile) {

    if (aSmile && bSmile) {
      return true;
    } else
      return !aSmile && !bSmile;
  }
}
