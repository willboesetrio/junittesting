package com.catalyte.training.monkeytesting;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for all possible monkey trouble cases
 */

public class MonkeyTest {

  private MonkeyTrouble monkeyTrouble;
  boolean monkeyA, monkeyB, expected, result;

  @BeforeEach
  public void init() {

    monkeyTrouble = new MonkeyTrouble();

  }

  @Test
  public void ReturnsTrueIfBothFalse() {

    monkeyA = false;
    monkeyB = false;
    result = monkeyTrouble.monkeyTrouble(monkeyA, monkeyB);
    expected = true;

    assertTrue(result, () -> "Incorrect result, expected " + expected);

  }

  @Test
  public void ReturnsTrueIfBothTrue() {

    monkeyA = true;
    monkeyB = true;
    result = monkeyTrouble.monkeyTrouble(monkeyA, monkeyB);
    expected = true;

    assertTrue(result, () -> "Incorrect result, expected " + expected);

  }

  @Test
  public void ReturnsFalseIfATrueAndBFalse() {

    monkeyA = true;
    monkeyB = false;
    result = monkeyTrouble.monkeyTrouble(monkeyA, monkeyB);
    expected = false;

    assertFalse(result, () -> "Incorrect result, expected " + expected);

  }

  @Test
  public void ReturnsFalseIfAFalseAndBTrue() {

    monkeyA = false;
    monkeyB = true;
    result = monkeyTrouble.monkeyTrouble(monkeyA, monkeyB);
    expected = false;

    assertFalse(result, () -> "Incorrect result, expected " + expected);

  }


}